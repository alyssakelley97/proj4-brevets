"""
Author: Alyssa Kelley

Disclaimer: I worked on this assignment with Anne Glickenhaus. No code was copied, but ideas
were shared. The logging lines were copied from the test files from project 3. 
I found all the tests from the first 4 test functions in the article that was 
provided to us (https://rusa.org/pages/acp-brevet-control-times-calculator). I thought it 
was best to use these test examples to ensure the assertions I was created were
actually correct. For the dates in the functions I just used my birthday since it was within
the year scope and allowed me to focus more on the hours/minutes being evaluated.  I then added
another test function (test_max) just to see if 1) the brevet and control values were the same
and 2) when it was the max value out of all the intervals. 
"""


import acp_times # Testing framework
import nose
import arrow
import logging

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)

log = logging.getLogger(__name__)

date = arrow.get(1997, 3, 14)

def test_example1():
	"""
	This test function illustrates the first example from the brevets document linked above. 
	The control_dist_km = 60km, the brevet_dist_k = 200 km and the brevet_start_time is
	the global date of 3/14/1997.
	The open time function should return the start time shifted by 1 hour and 46 minutes.
	The close time function should return the start time shifted by 4 hours.
	"""
	date_and_shifted_time1 = (date.shift(hours=1, minutes=46)).isoformat()
	assert acp_times.open_time(60, 200, date) == date_and_shifted_time1

	date_and_shifted_time2 = (date.shift(hours=4)).isoformat()
	assert acp_times.close_time(60, 200, date) == date_and_shifted_time2

def test_example2():
	"""
	This test function illustrates the second example from the brevets document linked above. 
	The brevet_dist_k = 600 km and the brevet_start_time is the global date of 3/14/1997.
	There are different control_dist_km's that are being tested. The open function is being
	tested against the controls of 100km, 200km, and 550km, and the close function is being 
	tested against the control of 550km.
	The open time function should return the start time shifted by 2 hours and 56 minutues
	when the control is 100km, time shifted 5 hours and 53 minutes when the control is 
	200km, and time shifted 17 hours and 8 minutes when the control is 550km. 
	The close time function should return the start time shifted by 36 hours and 40 minutes.
	"""
	date_and_shifted_time1 = (date.shift(hours=2, minutes=56)).isoformat()
	assert acp_times.open_time(100, 600, date) == date_and_shifted_time1

	date_and_shifted_time2 = (date.shift(hours=5, minutes=53)).isoformat()
	assert acp_times.open_time(200, 600, date) == date_and_shifted_time2

	date_and_shifted_time3 = (date.shift(hours=17, minutes=8)).isoformat()
	assert acp_times.open_time(550, 600, date) == date_and_shifted_time3

	date_and_shifted_time4 = (date.shift(hours=36, minutes=40)).isoformat()
	assert acp_times.close_time(550, 600, date) == date_and_shifted_time4


def test_example3():
	"""
	This test function illustrates the third example from the brevets document linked above. 
	The control_dist_km = 890km, the brevet_dist_k = 1000 km and the brevet_start_time is
	the global date of 3/14/1997.
	The open time function should return the start time shifted by 29 hours and 9 minutes.
	The close time function should return the start time shifted by 65 hours and 23 minutes.
	"""
	date_and_shifted_time1 = (date.shift(hours=29, minutes=9)).isoformat()
	assert acp_times.open_time(890, 1000, date) == date_and_shifted_time1

	date_and_shifted_time2 = (date.shift(hours=65, minutes=23)).isoformat()
	assert acp_times.close_time(890, 1000, date) == date_and_shifted_time2

def test_close_oddities():
	"""
	This test function illustrates the fourth example from the brevets document linked above
	which is talking about the hour oddity that occurs with the close time. This oddity
	is not a concern for the open function so this is only testing the close function. 
	The control_dist_km = 0km, the brevet_dist_k = 20 km and the brevet_start_time is
	the global date of 3/14/1997.
	The close time function should return the start time shifted by 1 hour when tested with 
	the control = 0km.
	"""
	date_and_shifted_time1 = (date.shift(hours=1)).isoformat()
	assert acp_times.close_time(0, 20, date) == date_and_shifted_time1


def test_max():
	"""
	This test case is the only one that did not come from the brevets document. I
	wanted to test to make sure if the max km was entered for the control and the brevet 
	distance. 
	"""
	date_and_shifted_time1 = (date.shift(hours=43, minutes=48)).isoformat()
	assert acp_times.open_time(1300, 1300, date) == date_and_shifted_time1

	date_and_shifted_time2 = (date.shift(hours=101, minutes=15)).isoformat()
	assert acp_times.close_time(1300, 1300, date) == date_and_shifted_time2

