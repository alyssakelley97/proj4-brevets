"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    #linked to calc.html to get the brevet distance, begin time, and begin date as variables to use as pass to acp_times
    distance = request.args.get('distance', 0, type = int)
    start_time = request.args.get('start_time', "", type = str)
    start_date = request.args.get('start_date', "", type = str)
    print("This is the starting time: ", start_time)
    print("This is the starting date: ", start_date)
    print("THis is the distance: ", distance)

    # adding the date and time together and ensuring it is in the correct argument format to pass to the acp_times functions
    starting_time = arrow.get(start_date + " " + start_time).isoformat()
    print("Starting time = ", starting_time)

    open_time = acp_times.open_time(km, distance, starting_time)
    print("--> This is what open_time becomes = ", open_time)
    close_time = acp_times.close_time(km, distance, starting_time)
    print("--> This is what close_time becomes = ", close_time)
    
    # Displaying the open and close times as they already are formatted
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
