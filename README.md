# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Author: Alyssa Kelley, alyssak@uoregon.edu

## Information regarding the concepts of the project 

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

This project is  essentially replacing the calculator here (https://rusa.org/octime_acp.html).

## How the ACP Times Algorithms work

# Open Time Algorithm 
Arguments: control_dist_km, brevet_dist_km, brevet_start_time
Returns: control open time, meaning the brevet_start_time shifted by the amount of time spent

In the Rusa brevet documentation listed above, it specifies that the calculation of opening time is based on the max speed. These max speeds
are associated to the different km intervals of 0-200, 200-400, 400-600, 600-1000, and 1000-1300. As seen, there are only 5 intervals that can
be used, and only 5 max speeds. Here are the maximum speeds: 34.0, 32.0, 30.0, 28.0, 26.0 

To calculate the amount of time that is spent, we need to take the control distance km and loop through that going through each interval
and to find the current time spent for that interval, we need to find the corresponding max speed. Then if we do 200 diveded by the current
max speed, that number will be the amount of hours spent for that interval. We can accumulate all these hours spent, and that number will be
the amount of time we will add to the start time to get the final return value. However, we need the hours to be in minutes. This is simply 
done by take the integer value of the hours mutiplied by 60. I found, with the help of fellow classmates, that you get a more accurate and 
consistent minute result when also adding .5 to the number after multipling it and before converting it to an int.

Once you have the amount in minutes, you simply shift the start date by that amount of minutes to get the final open time.

# Close Time Algorithm
Arguments: control_dist_km, brevet_dist_km, brevet_start_time
Returns: control open time, meaning the brevet_start_time shifted by the amount of time spent

In the Rusa brevet documentation listed above, it specifies that the calculation of opening time is based on the min speed. These min speeds
are associated to the different km intervals of 0-200, 200-400, 400-600, 600-1000, and 1000-1300. As seen, there are only 5 intervals that can
be used, and only 5 min speeds. Here are the minimum speeds: 15.0, 15.0, 15.0, 11.428, 13.333

The algorithm for finding the close time is the same as when finding the open time expect for using the min speed instead of the max speed. There is another step needed to take for the close time regarding the oddities section of the examples listed in the Rusa brevets documentation linked above.
This oddity is regarding when the control is 0km, the close time should return one hour. To account for this oddity, I included an additional check at
the beginning of the function.

## How all the files work together

The flask file helped the frontend talk to the backend. The flask file gets the begin_time, begin_date and the brevet_dist_km from the calc.html file. 
The AJAX is implemented in the calc.html to pull these variables out and pass them to the functions so that the flask file can use them, and pass them
to the acp_times API (acp_times.py). After the flask file passes these arguments to the correct functions and recieves the proper return times, it returns those results using jsonify to populate the open and close time columns on the webpage.

## How to test

In the brevets directory, there is a subdirectory called "tests". In this directory there is a file "test_acp_times.py" that contains 5 different test 
functions to confirm that the acp_times open and close function are working correctly. Four out of five of these test functions use the exact examples
that are listed in the brevet documentation listed above. I found it important to use these specific examples in my test script because they were 
important enough to be in the official documentation for the algorithm, and I was assured that the final return time was exactly as expected which leaves no room for hand calculation mistakes. I then included one test that just tests when the km is at its absolute max point for the control and for the brevet. 

To run the nosetest, go to the brevets directory and run the command "nosetests"

